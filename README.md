Portainer
=======

This is a container manager called [Portainer](https://portainer.io/)

##### Installation  

1. Clone this repository

    ```bash
    $ git clone git@bitbucket.org:syneteksolutions/portainer.git	 
    ```

3. Start the services

    ```bash
    $ cd portainer
    $ docker-compose up -d --build
    ```	
	
5. Portainer will be available on port `9000`

